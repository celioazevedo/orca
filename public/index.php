<?php

defined('APPLICATION_PATH')
	|| define('APPLICATION_PATH', realpath(dirname(__FILE__).'/../application'));

defined('LIBRARY_PATH')
	|| define('LIBRARY_PATH', realpath(dirname(__FILE__).'/../library'));

require_once LIBRARY_PATH.'/Orca/Application/Application.Class.php';

$application = new Orca_Application_Create(APPLICATION_ENV, APPLICATION_PATH.'/configs/application.ini');

$application->runTheMagic();