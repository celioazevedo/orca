<?php
class Orca_Layout_Create
{
	private static $instance;
	private $layoutStatus = true;
	public $layout;
	public $view;
	
	public function __construct()
	{
		
	}
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function disable(){
		$this->layoutStatus = false;
	}
	
	public function setLayout($layout = 'default.otpl'){
		$this->layout = $layout;
	}
	
	public function createLayout(){
		if($this->layoutStatus){
			if(file_exists(APPLICATION_PATH.'/views/layouts/'.$this->layout)){
				require_once APPLICATION_PATH.'/views/layouts/'.$this->layout;
			}else{
				throw new Orca_Exception_Create('Layout file ("'.$this->layout.'") not found.');
			}
		}else{
			$this->content();
		}
	}
	
	public function content()
	{
		echo $this->view;
	}
	
	public function assign($var, $value = null)
	{
		$this->$var = $value;
	}
}