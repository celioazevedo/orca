<?php

class Orca_View_Create
{
	private static $instance;
	private $viewStatus = true;
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function startViewContent(Orca_Controller_Create $controller)
	{	
		return $this->render($controller);
	}
	
	public function disable()
	{
		$this->viewStatus = false;
	}
	
	protected function render($controller)
	{
		if($this->viewStatus){
			ob_start();
			if(file_exists(APPLICATION_PATH.'/views/scripts/'.$controller->getController().'/'.$controller->getAction().'.otpl')){
				require_once APPLICATION_PATH.'/views/scripts/'.$controller->getController().'/'.$controller->getAction().'.otpl';
				$conteudo = ob_get_clean();
				return $conteudo;	
			}else{
				throw new Orca_Exception_Create('Invalid view specified ("'.$controller->getController().'/'.$controller->getAction().'.otpl'.'").', 0);
			}
				
		}
	}
	
	public function assign($var, $value = null)
	{
		$this->$var = $value;
	}
}