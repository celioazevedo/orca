<?php
class Orca_Application_Create
{
	protected $_environment;
	protected $_applicationIni;
	protected $controls;
	public static $view;
	
	public function __construct($environment, $applicationIni)
	{	
		$this->_environment = (string) $environment;
		
		$this->_applicationIni = (string) $applicationIni;
		
		spl_autoload_register(array($this, 'autoloader'));
	}
	
	public function getEnvironment()
	{
		return $this->_environment;
	}
	
	public function getApplicationIni()
	{
		return $this->_applicationIni;
	}
	
	
	public function autoloader($class){
		
		$paths = explode('_', $class);
		$path = '';
		if(current($paths) === 'Orca'){
			
			foreach ($paths as $pathPart){
				if($pathPart !== 'Create'){
					$path .= DIRECTORY_SEPARATOR.$pathPart;
					$party = $pathPart;
				}else{
					$path .= DIRECTORY_SEPARATOR.$party.'.Class.php';
				}
			}
			$path = LIBRARY_PATH.$path;
		}else{
			if(current($paths) === 'Application'){
				foreach ($paths as $pathPart){
					if($pathPart === 'Model'){
						$path .= DIRECTORY_SEPARATOR.'models';
					}elseif($pathPart === 'Forms'){
						$path .= DIRECTORY_SEPARATOR.'forms';
					}elseif($pathPart !== 'Application'){
						$path .= DIRECTORY_SEPARATOR.$pathPart;
					}
				}
				$path = APPLICATION_PATH.$path.'.php';
			}else{
				$path = APPLICATION_PATH.'/controllers/'.$class.'.php';
			}
		}

		require_once $path;
	}

	
	protected function loadAppIni($applicationIni)
	{	
		$config = new Orca_Config_Ini_Create($applicationIni);
	}
	
	/*protected function setController()
	{
		$this->controllerName = $this->controls->getController();
		$this->controllerNomenclature = ucfirst(strtolower($this->controllerName)).'Controller';
		if(file_exists(APPLICATION_PATH.'/controllers/'.$this->controllerNomenclature.'.php')){
			$this->controller = new $this->controllerNomenclature;
		}else{
			throw new Orca_Exception_Create('Invalid controller specified ("'.$this->controllerName.'").', 0);
		}
	}*/
	
	/*protected function setAction()
	{
		$this->actionName = $this->controls->getAction();
		$actionNomenclature = $this->actionNomenclature = ucfirst(strtolower($this->actionName)).'Action';
		$this->controller->$actionNomenclature();
	}*/
	
	public function setView()
	{
		$this->view = $this->controllerName.'/'.$this->actionName.'.otpl';
		$this->view = APPLICATION_PATH.'/views/scripts/'.$this->view;
	}
	
	public static function getView(){
		return self::$view;
	}
	
	public function runTheMagic()
	{
		$this->loadAppIni($this->getApplicationIni());
		
		$this->controls = Orca_Controller_Create::getInstance();	
		
		/*$layout = new Orca_Controller_Action_Create();
		
		if(!$layout->layout()->setLayout()){
			$layout->layout()->setLayout();
		}
		$layout->layout()->createLayout();*/
		
	}
}