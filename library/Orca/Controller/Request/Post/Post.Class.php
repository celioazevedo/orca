<?php
class Orca_Controller_Request_Post_Create
{
	private static $instance;
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function isPost()
	{
		return $_SERVER['REQUEST_METHOD'] === 'POST' ? true : false;
	}
	
	public function getPost($value = null, $default = null)
	{
		if(is_null($value)){
			return $_POST;
		}elseif(isset($_POST[$value])){
			return $_POST[$value];
		}else{
			return $default;
		}
	}
}