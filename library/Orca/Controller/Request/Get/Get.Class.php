<?php

class Orca_Controller_Request_Get_Create
{
	private $params = array();
	
	private static $instance;
	
	private function __construct()
	{
		if(!empty($_GET)){
			foreach($_GET as $key => $value) {
				$this->setParam(array(urldecode($key) => urldecode($value)));
			}
		}
	}
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function isGet()
	{
		return $_SERVER['REQUEST_METHOD'] === 'GET' ? true : false;
	}
	
	public function setParam($param)
	{
		if(is_array($param) || is_object($param)){
			if(key($param)) {
				$this->params[key($param)] = current($param);
			} else {
				array_push($this->params, $param);
			}
		}else{
			array_push($this->params, $param);
		}
	}
	
	public function getAllParams()
	{
		return $this->params;
	}
	
	public function getParam($value = null, $default = null)
	{
		if(is_null($value)){
			return $default;
		}elseif(isset($this->params[$value])){
			return $this->params[$value];
		}else{
			return $default;
		}
	}
}