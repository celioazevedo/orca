<?php
class Orca_Controller_Action_Create
{
	public function layout()
	{
		return Orca_Layout_Create::getInstance();
	}

	public function view()
	{
		return Orca_View_Create::getInstance();
	}
	
	public function getParam($value = null, $default = null)
	{
		$param = Orca_Controller_Request_Get_Create::getInstance();
		return $param->getParam($value, $default);
	}
	
	public function getAllParams()
	{
		$param = Orca_Controller_Request_Get_Create::getInstance();
		return $param->getAllParams();
	}
	
	public function getPost(){
		$post =  Orca_Controller_Request_Post_Create::getInstance();
		return $post->getPost();
	}
	
	public function isPost(){
		$post =  Orca_Controller_Request_Post_Create::getInstance();
		return $post->isPost();
	}
}