<?php

class Orca_Controller_Create
{
	
	protected $mainPath;
	
	protected static $instance;
	
	protected static $controller;
	
	protected static $controllerName;
	
	protected static $controllerFile;

	protected static $action;
	
	protected static $actionName;

	protected static $view;
		
	
	public function __construct()
	{
		$this->_setMainPath();
		$this->_setControllerAndAction();
		
		if((bool) $this->createController()){
			
			if((bool) $this->createAction()){
				
				$view = Orca_Layout_Create::getInstance();
				$view->view = $this->createView();
				
				$layout = new Orca_Controller_Action_Create();

				if(!$layout->layout()->layout){
					$layout->layout()->setLayout();
				}
				$layout->layout()->createLayout();
				
			}
			
		}
		
	}
	
	protected function setMainPath($path)
	{
		$this->mainPath = $path;
		return $this;
	}
	
	public  function getMainPath()
	{
		return $this->mainPath;
	}
	
	protected function setControllerName($controller)
	{
		$this->controller->name = $controller;
		return $this;
	}
	
	public  function getController()
	{
		return $this->controller->name;
	}
	
	protected function setActionName($action)
	{
		$this->action = $action;
		return $this;
	}
	
	public  function getAction()
	{
		return $this->action;
	}
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	protected function _setMainPath()
	{
		$base = array();
		
		$docRoot = explode('/', $_SERVER['DOCUMENT_ROOT']);
		
		$nPhpSelf = '';
		
		$scriptFileName = explode('/', $_SERVER['SCRIPT_FILENAME']);
		
		foreach($scriptFileName as $sFN) {
			if(!in_array($sFN, $docRoot)) $nPhpSelf = '/'.$sFN;
		}
		
		$phpSelf =
			preg_match('/\.(php)$/', $_SERVER['PHP_SELF']) ?
				$_SERVER['PHP_SELF']
			:
				$nPhpSelf;
				
		foreach(explode('/', $phpSelf) as $field) {
			if(!empty($field) && !preg_match('/\.(php)$/', $field)) {
				//if(Codepass_Hand::testPathName($field)) {
					$base[] = $field;
				//}
			}
		}
		
		$paths = implode('/', $base);
		
		$this->mainPath = !empty($paths) ? '/'.$paths : '';
	}
	
	protected function _setControllerAndAction()
	{
	$dispatch = explode(
			'/',
			strtr(
				trim(preg_replace(
					'/(^\/+)/',
					'',
					urldecode($_SERVER['REQUEST_URI'])
				)),
				'áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ',
			'aaaaeeiooouucAAAAEEIOOOUUC')
		);
		
		$count = 0;

		$paramConfig = null;
		$paths = array();
		foreach($dispatch as $dKey => $dField)
		{
			if(!in_array($dField, explode('/', $this->getMainPath())))
			{
				switch($count) {
					case 0:
						if($this->getController() === null) {
							$this->setControllerName($dField);
						}
					break;
					case 1:
						if($this->getAction() === null) {
							$this->setActionName($dField);
						}
					break;
					default:
						if(preg_match('/\?|\=|\&/', $dField)) {
							$count--;
						} else {
							$param = Orca_Controller_Request_Get_Create::getInstance();
							if($count%2 == 0) {
								$paramConfig = $dField;
								$param->setParam(array($paramConfig => null));
							} else {
								$param->setParam(array($paramConfig => $dField));
							}
						}
					break;
				}
				$paths[] = $dField;
				$count++;
			}
		}
		
		// Routes Ini File
		$routes = Orca_Config_Ini_Create::getIniArray();
		if(isset($routes['routes'])){
			$routes = $routes['routes'];
			foreach ($routes as $key => $value){
				$pathRoute = true;
				$params = array();
				$actualRoute = explode('/', $value['route']);
				foreach ($actualRoute as $nKey => $item){
					if(substr(trim($item), 0, 1) != ':'){
						if($item != $paths[$nKey]){
							$pathRoute = false;
						}
					}else{
						$params[substr(trim($item), 1)] = $paths[$nKey];
					}
				}
				if($pathRoute){
					$param = Orca_Controller_Request_Get_Create::getInstance();
					foreach ($params as $index => $val){
						$param->setParam(array($index => $val));
					}
					$this->setControllerName($value['controller']);
					$this->setActionName($value['action']);
					break;
				}
			}
		}else{
			unset($routes);
		}
		
		if($this->getController() === null) {
			$this->setControllerName('index');
		}
		
		if($this->getAction() === null) {
			$this->setActionName('index');
		}

	}
	
	protected function createController()
	{
		$this->controllerName =  ucfirst(strtolower($this->getController())).'Controller';
		$this->controllerFile =  APPLICATION_PATH.'/controllers/'.$this->controllerName.'.php';
		if(file_exists($this->controllerFile)){
			
			require_once $this->controllerFile;
			
			if(class_exists($this->controllerName, null)){
				$this->controller->instance = new $this->controllerName;
				return true;
			}else{
				throw new Orca_Exception_Create('Invalid class specified ("'.$this->controllerName.'") on file.'.
				$this->controllerFile, 1);
			}
		}else{
			throw new Orca_Exception_Create('Invalid controller specified ("'.$this->controllerFile.'").', 0);
		}
		return false;
	}
	
	protected function createAction()
	{
		$this->actionName = ucfirst(strtolower($this->getAction())).'Action';
		$actionName = $this->actionName;
		if(method_exists($this->controller->instance, $actionName)){
			$this->controller->instance->$actionName();
			return true;
		}else{
			throw new Orca_Exception_Create('Invalid action(Method) specified ("'.$this->actionName.'").', 2);
		}
		return false;
	}
	
	public function createView()
	{
		$this->view = Orca_View_Create::getInstance();
		return $this->view->startViewContent($this);
		
	}
}