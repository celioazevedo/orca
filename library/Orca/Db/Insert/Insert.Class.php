<?php
class Orca_Db_Insert_Create extends Orca_Db_Create
{	
	private static $instance;
	
	private $dbTable;
	
	private $sql;
	
	const SEPARATOR = ' ';
	
	const INSERT = 'INSERT';
	
	const INTO = 'INTO';
	
	const VALUES = 'VALUES';
	
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function setDbTable($dbTable)
	{
		$this->dbTable = $dbTable;	
	}
	
	public function getDbTable()
	{
		return $this->dbTable;
	}
	
	private function createSql($val)
	{
		$sql = self::INSERT.self::SEPARATOR.self::INTO.self::SEPARATOR.
		$this->tables[] = $schema.'`'.$this->getDbTable()->TbName.'`';
		
		$sql .= self::SEPARATOR.'(';
		
		if(is_array($val)){
			$x = 0;
			foreach ($val as $key => $value){
				$sql .= (($x == 0)? '' : ',').self::SEPARATOR.$key;
				$x++;
			}
		}
		$sql .= self::SEPARATOR.')';
		$sql .= self::SEPARATOR.self::VALUES.'(';
		
		if(is_array($val)){
			$x = 0;
			foreach ($val as $key => $value){
				$sql .= (($x == 0)? '' : ',').self::SEPARATOR.$value;
				$x++;
			}
		}
		$sql .= self::SEPARATOR.')';
		
		return $sql;
	}
	
	public function insert($val)
	{	
		$sql = $this->createSql($val);
		$this->prepare($sql);
		$this->execute();
		return $this->lastId();
	}
	public function __toString()
	{	
		return $this->createSql();
	}
	
}