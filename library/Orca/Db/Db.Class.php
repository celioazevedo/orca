<?php
class Orca_Db_Create
{
	private static $instance;
	
	private $dbConfigs;
	
	private $dsn;
	
	private $pdo;
	
	private $query;
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function __construct()
	{
		$this->setDbConfigs();
		$this->setDsn();
	}
	
	private function connect()
	{	
		try{
			
			$this->pdo = new PDO(
			$this->getDsn(), 
			$this->getConfig('user'), 
			$this->getConfig('password'));
			
		} catch (PDOException $e) {
			throw new Orca_Exception_Create('Cannot connect. Error: '.$e->getMessage());
		}
		
	}
	private function getDsn()
	{
		return $this->dsn;
	}
	
	private function setDsn()
	{
		$this->dsn = $this->getConfig('driver').':host='.$this->getConfig('host').';port='.
		$this->getConfig('port').';dbname='.$this->getConfig('dbname');
	}
	
	private function setDbConfigs()
	{
		$this->dbConfigs = Orca_Config_Ini_Create::getIniArray();
		$this->dbConfigs = $this->dbConfigs['dbconfigs']['config'];		
	}
	
	private function getConfig($config)
	{
		return $this->dbConfigs[$config];
	}
	
	public function getConnection()
	{
		$this->connect();
		return $this->pdo;
	}
	
	public function prepare($sql, $params = null)
	{
		$con = $this->getConnection();
		$this->query = $con->prepare($sql);
		if(is_array($params)){
			foreach ($params as $key => $param){
				$this->query->bindParam($key+1, $param['value']);
			}			
		}
	}
	
	public function execute()
	{
		return $this->query->execute();
	}
	
	public function lastId()
	{
		return $this->pdo->lastInsertId();
	}
	
	public function fetch()
	{
		return $this->query->fetchAll(PDO::FETCH_ASSOC);
	}
	
}