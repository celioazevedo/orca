<?php

class Orca_Db_DbTable_Create
{
	protected $dbTable;

    protected function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
            
        }
        /*if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }*/
        $this->dbTable = $dbTable;
        return $this;
    }

    protected function getDbTable()
    {
		$clasPartsName = explode('_', get_class($this));
		end($clasPartsName);
		$tableName = str_replace('Mapper', '', current($clasPartsName));
		
        if (null === $this->dbTable) {
            $this->setDbTable('Application_Model_DbTable_'.$tableName);
        }
        $dbTableAbstract = Orca_Db_DbTable_Abstract_Create::getInstance();
        $dbTableAbstract->setDbTable($this->dbTable);
    	return $dbTableAbstract;
    }
    
	protected function save($obj, $where = null)
    {
        if (null === ($id = $obj->getId())) {
            $this->getDbTable()->insert($obj);
        } else {
            $this->getDbTable()->update($obj, $where);
        }
    }
   
    protected function fetchAll()
    {
    	 $resultSet = $this->getDbTable()->fetchAll();
    	 return $resultSet->toArray();
    }
}