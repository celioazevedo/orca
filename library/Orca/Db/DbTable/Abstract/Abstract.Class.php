<?php
class Orca_Db_DbTable_Abstract_Create
{
	private static $instance;
	
	private $dbTable;
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function setDbTable($dbTable)
	{
		$this->dbTable = $dbTable;	
	}
	
	public function getDbTable()
	{
		return $this->dbTable;
	}
	
	
	public function select()
	{	
		$dbSelect = new Orca_Db_Select_Create;
		$dbSelect->setDbTable($this->getDbTable());
		return $dbSelect;
	}
	
	public function fetchAll($sql)
	{
		$dbSelect = new Orca_Db_Select_Create;
		return $dbSelect->fetchAll($sql);
	}
	
	public function insert($val)
	{
		$dbInsert = new Orca_Db_Insert_Create();
		$dbInsert->setDbTable($this->getDbTable());
		return $dbInsert->insert($val);
	}
	
	public function update($val)
	{
		$dbUpdate = new Orca_Db_Update_Create();
		$dbUpdate->setDbTable($this->getDbTable());
		return $dbUpdate->update($val);
	}
	
	public function delete($val)
	{
		$dbDelete = new Orca_Db_Delete_Create();
		$dbDelete->setDbTable($this->getDbTable());
		return $dbDelete->delete($val);		
	}
}