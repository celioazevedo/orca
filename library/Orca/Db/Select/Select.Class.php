<?php
class Orca_Db_Select_Create extends Orca_Db_Create
{	
	private static $instance;
	
	private $dbTable;
	
	private $sql;
	
	private $fields;
	
	private $tables;
	
	private $where;
	
	private $group;
	
	private $order;
	
	private $limit;
	
	const SEPARATOR = ' ';
	
	const SELECT = 'SELECT';
	
	const ALLFIELDS = '*';
	
	const ALIAS = 'AS';
	
	const FROM = 'FROM';
	
	const WHERE = 'WHERE';
	
	const sAND = 'AND';
	
	const sOR = 'OR';

	const LIMIT = 'LIMIT';
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function setDbTable($dbTable)
	{
		$this->dbTable = $dbTable;	
	}
	
	public function getDbTable()
	{
		return $this->dbTable;
	}
	
	public function from($tb = null, $fields = null)
	{
		if($this->dbTable->schema !== null){
			$schema = '`'.$this->dbTable->schema.'`.';
		}
		if($tb === null){
			$this->tables[] = $schema.'`'.$this->getDbTable()->TbName.'`';
		}else{
			if(is_array($tb)){
				foreach ($tb as $key => $nTb){
					if(!is_numeric($key)){
						$this->tables[] = $schema.'`'.$nTb.'`'.self::SEPARATOR.$key;
					}else{
						$this->tables[] = $schema.'`'.$nTb.'`';
					}
				}
			}else{
				$this->tables[] = $schema.'`'.$tb.'`';
			}
		}
		if($fields === null){
			$this->fields[] = '*';
		}else{
			if(is_array($fields)){
				foreach ($fields as $key => $nF){
					if(!is_numeric($key)){
						$this->fields[] = $nF.self::SEPARATOR.self::ALIAS.self::SEPARATOR.$key;
					}else{
						$this->fields[] = $nF;
					}
				}
			}else{
				$this->fields[] = $fields;
			}
		}
		return $this;
	}
	public function join($table, $cond, $fields = null)
	{
		
	}
	
	public function where($column, $value = null, $type = null)
	{
		$this->createWhere($column, $value, $type, self::sAND);
		return $this;
	}
	
	
	
	public function orWhere($column, $value = null, $type = null)
	{
		$this->createWhere($column, $value, $type, self::sOR);
		return $this;
	}
	
	private function createWhere($column, $value = null, $type = null, $cond)
	{
		$column = ((isset($this->where[0]))? $cond.self::SEPARATOR : self::SEPARATOR).$column;
		$this->where[] = array('column' => $column, 
								'value' => $value,
								'type' => ($type === null)? 'string' : $type);
	}
	
	public function groupBy($group)
	{
		$this->group[] = $group;
		return $this;
	}
	
	public function orderBy($order)
	{
		$this->order[] = $order;
		return $this;
	}
	
	public function limit($count, $offset = null)
	{
		$this->limit['count'] = $count;
		$this->limit['offset'] = ($offset === null)? '' : $offset;
		return $this;
	}
	
	private function createSql()
	{
		$sql = self::SELECT;
		
		$sql .= self::SEPARATOR.$this->renderFields();
		
		$sql .= self::SEPARATOR.$this->renderTables();
		
		$sql .= self::SEPARATOR.$this->renderWhere();
		
		$sql .= self::SEPARATOR.$this->renderWhere();
		
		if($this->limit){
			$sql .= self::SEPARATOR.$this->renderLimit();
		}
		return $sql;
	}
	
	private function renderTables()
	{
		$sql = self::FROM.self::SEPARATOR.implode(', ', $this->tables);
		return $sql;
	}
	
	private function renderFields()
	{
		$sql = implode(', ', $this->fields);
		return $sql;
	}
	
	private function renderWhere()
	{
		if(is_array($this->where)){
			foreach ($this->where as $cond){
				$where[] = $cond['column'];
			}
			$sql = self::WHERE.self::SEPARATOR.implode(' ', $where);
		}
		return $sql;
	}
	
	private function renderLimit()
	{
		$sql = self::LIMIT.self::SEPARATOR.(($this->limit['offset'])? $this->limit['offset'].','.self::SEPARATOR : '').$this->limit['count'];
		return $sql;
	}
	
	public function fetchAll($sql)
	{
		$this->prepare($sql, $this->where);
		$this->execute();
		return $this->fetch();
	}
	public function __toString()
	{	
		return $this->createSql();
	}
	
}