<?php
class Orca_Db_Delete_Create extends Orca_Db_Create
{
	private static $instance;
	
	private $dbTable;
	
	private $sql;
	
	const SEPARATOR = ' ';
	
	const DELETE = 'DELETE';
	
	const FROM = 'FROM';
	
	const WHERE = 'WHERE';
	
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	public function setDbTable($dbTable)
	{
		$this->dbTable = $dbTable;	
	}
	
	public function getDbTable()
	{
		return $this->dbTable;
	}
	
	private function createSql($val)
	{
		$sql = self::DELETE.self::SEPARATOR.self::FROM.self::SEPARATOR.
		$this->tables[] = $schema.'`'.$this->getDbTable()->TbName.'`';
		$sql .= self::SEPARATOR.self::WHERE.self::SEPARATOR;
		$sql .= key($val).self::SEPARATOR.' = '.$val[key($val)];
		
		
		return $sql;
	}
	
	public function delete($val)
	{	
		$sql = $this->createSql($val);
		$this->prepare($sql);
		return $this->execute();
	}
	public function __toString()
	{	
		return $this->createSql();
	}

}