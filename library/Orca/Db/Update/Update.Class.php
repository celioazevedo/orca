<?php
class Orca_Db_Update_Create extends Orca_Db_Create
{	
	private static $instance;
	
	private $dbTable;
	
	private $sql;
	
	private $where;
	
	const SEPARATOR = ' ';
	
	const UPDATE = 'UPDATE';
	
	const SET = 'SET';
	
	const VALUES = 'VALUES';
	
	const sAND = 'AND';

	const sOR = 'OR';
	
	const WHERE = 'WHERE';
	
	
	public static function getInstance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function setDbTable($dbTable)
	{
		$this->dbTable = $dbTable;	
	}
	
	public function getDbTable()
	{
		return $this->dbTable;
	}
	
	private function createSql($val)
	{
		$sql = self::UPDATE.self::SEPARATOR.
		$this->tables[] = $schema.'`'.$this->getDbTable()->TbName.'`'.self::SEPARATOR.
		self::SET;
		
		$sql .= self::SEPARATOR;
		
		if(is_array($val)){
			$x = 0;
			foreach ($val as $key => $value){
				$sql .= (($x == 0)? '' : ',').self::SEPARATOR.$key.self::SEPARATOR.' = '.$value;
				$x++;
			}
		}
		$sql .= self::SEPARATOR;
		
		$sql .= self::WHERE.self::SEPARATOR;
		$sql .= 'id = '.$val['id'];
		return $sql;
	}
	

	public function update($val)
	{	
		$sql = $this->createSql($val);
		$this->prepare($sql);
		return $this->execute();
	}
	public function __toString()
	{	
		return $this->createSql();
	}
	
}