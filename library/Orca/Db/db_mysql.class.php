<?php

// Gamistik Softwares
// Classe: CONEXAO
// Descri��o: Conecta ao banco de dados MySQL, executa, query�s SQL�s, entre outros recursos para banco de dados.
// Autor: Celio S. Azevedo
// Data do Inicio da Cria��o: 05/09/2010.
// Data do Fim da Cria��o: 05/09/2010.
// Data da Ultima Modifica��o. 05/09/2010

class conexao extends defines_db{
	
	var $SQL = null;
	var $linha = null;
	var $arquivo = null;
	var $resultado = null;
	var $dados = null;
	var $linhas = null;
	var $fetched = false;
	//var $servidor_db = "localhost";
	
	// CONECTA_SERVIDOR: Realiza comunica��o com o servidor.
	function conecta_servidor(){
		
		if(!(@mysql_connect($this->servidor_db, $this->user_db, $this->password_db))){
			$this->relatorio_erro_mysql(mysql_errno());
			die();
		}		
		
	}
	
	// CONECTA_DB: Realiza comunica��o com o banco de dados.
	function conecta_db(){
		
		if(!(@mysql_select_db($this->data_base))){
			$this->relatorio_erro_mysql(mysql_errno());
			die();
		}		
		
	}
	
	// CONECTAR: chama a fun��o �conecta_servidor� e a �conecta_db� e estabelece conex�o com o banco de dados no servidor.
	function conectar(){
		
		$this->conecta_servidor();
		$this->conecta_db();
		
	}
	
	function ExecSQL(){
		$this->conectar();
		if($this->useUtf8){
			mysql_query('SET character_set_client=utf8');
		}		
		$this->resultado = mysql_query($this->SQL);
		if(!($this->resultado)){
			$this->relatorio_erro_mysql(mysql_errno());
			return false;	
		}else{
			if(strtolower(substr($this->SQL, 0, 6))== "select"){
				$this->dados_fetch();
				$this->row();
			}
				return true;
		}
		
	}
	
	function dados_fetch() {
		$i = 0; 
		while($row = mysql_fetch_array($this->resultado, MYSQL_ASSOC)){
			$this->dados[$i] = $row;
			$i++;
		}
	}
	
	function row(){
		$this->linhas = mysql_num_rows($this->resultado);
		if($this->linhas){
			$this->fetched = true;
		}
	}
	
    function ultimo_id()
    {
        return mysql_insert_id();
    }
    
	function relatorio_erro_mysql($erro_no){ 
		$rel = null;
		$rel = "<b>Relat�rio de erros SQL</b>";
		$rel .= "<br>--------------------------------------------------------------------------------------------------------------";
		$rel .= "<br>Data - Hora: ".date("d/m/Y - H:i:s"); 
		$rel .= "<br>--------------------------------------------------------------------------------------------------------------"; 
		$rel .= "<br> <b>Erro N�mero:</b> ".$erro_no;
		$rel .= "<br> <b>Erro:</b> ". $this->message_erro_sql();
		$rel .= "<br><b>Linha:</b> ".(($this->linha) + 1);
		$rel .= "<br><b>Arquivo:</b> '".$this->arquivo."'";
		$rel .= "<br>--------------------------------------------------------------------------------------------------------------";
		$rel .= "<br><b>String SQL:</b> <br>".$this->formata_sql_erro();
		$rel .= "<br>--------------------------------------------------------------------------------------------------------------";
		$rel .= "<br><b>Vari�veis do tipo GET:</b>";
		$rel .= "<pre>".print_r($_GET, 1)."</pre>";
		$rel .= "<br><b>Vari�veis do tipo POST:</b>";
		$rel .= "<pre>".print_r($_POST, 1)."</pre>";
		$rel .= "<br><b>Vari�veis de COOKIES:</b>";
		$rel .= "<pre>".print_r($_COOKIE, 1)."</pre>";
		$rel .= "<br><b>Vari�veis do tipo SESSION:</b>";
		$rel .= "<pre>".print_r($_SESSION, 1)."</pre>";
		echo "<P><span style='font-size: 20px; font-family: courier new,courier,monospace;'>".$rel."</font></P>";
	}
	
	function message_erro_sql() {
		$erro_sql = mysql_error();
		$erro_sql = str_replace("Table", "A Tabela", $erro_sql);
		$erro_sql = str_replace("doesn't exist", "n�o existe.", $erro_sql);
		$erro_sql = str_replace("Unknown column", "Coluna desconhecida na lista de campos: ", $erro_sql);
		$erro_sql = str_replace("Unknown database", "Banco de Dados desconhecido: ", $erro_sql);
		$erro_sql = str_replace("in 'field list'", "", $erro_sql);
		$erro_sql = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near", " Existe um erro na sintaxe da QUERY SQL, pr�ximo �:", $erro_sql);
		$erro_sql = str_replace("at line", "- Na linha:", $erro_sql);
		return $erro_sql;
	}
	
	function formata_sql_erro() {
		$sql = $this->SQL;
		$sql = str_replace("SELECT", "<BR><B>SELECT</B><BR>", $sql);
		$sql = str_replace("FROM", "<BR><B>FROM</B><BR>", $sql);
		$sql = str_replace("WHERE", "<BR><B>WHERE</B><BR>", $sql);
		$sql = str_replace("ORDER BY", "<BR><B>ORDER BY</B><BR>", $sql);
		$sql = str_replace("GROUP BY", "<BR><B>GROUP BY</B><BR>", $sql);
		$sql = str_replace("HAVING", "<BR><B>HAVING</B><BR>", $sql);
		$sql = str_replace("DELETE", "<BR><B>DELETE</B><BR>", $sql);
		$sql = str_replace("INSERT INTO", "<BR><B>INSERT INTO</B><BR>", $sql);
		$sql = str_replace("UPDATE", "<BR><B>UPDATE</B><BR>", $sql);
		$sql = str_replace("SET", "<BR><B>SET</B><BR>", $sql);
		$sql = str_replace("VALUES", "<BR><B>VALUES</B><BR>", $sql);
		$sql = str_replace("INNER JOIN", "<BR><B>INNER JOIN</B><BR>", $sql);
		$sql = str_replace("LEFT JOIN", "<BR><B>LEFT JOIN</B><BR>", $sql);
		return $sql;
	}
}
?>