<?php
class Orca_Upload_Create
{
	private $files = array();
	
	private $fileType = array();
	
	private $maxFileSize = 0;
	

	public function addFile($file){
			foreach ($file as $key => $f){
				if(!is_numeric($key)){
					$f['name'] = $key;
				}
				$this->files[] = $f;
			}
	}
	
	public function addFileType($type){
		if(is_array($type)){
			foreach ($type as $fT){
				$this->fileType[] = $fT;
			}
		}else{
			$this->fileType[] = $type;
		}
		
		return $this;
	}
	
	public function maxFileSize($size){
		$this->maxFileSize = $size;
		return $this;
	}
	
	
	public function upload($path){
		foreach ($this->files as $key => $file){
			if($file["error"] > 0){
				switch ($file["error"]){
					case 1:
						echo "";
						break;
					case 2:
						echo "";
						break;
					case 3:
						echo "";
						break;
					case 4:
						echo "";
						break;
					case 6:
						echo "";
						break;
					case 7:
						echo "";
						break;
					case 8:
						echo "";
						break;
				}
			}elseif(!$this->validateExt($this->getExt($file["name"]))){
				echo "erro ext";
			}elseif(!$this->validateMaxFileSize($file["size"])){
				echo "erro tamanho";
			}elseif(!is_uploaded_file($file['tmp_name'])){
				echo "erro 1";
			}elseif(!move_uploaded_file($file['tmp_name'], $path.$file['name'])){
				echo "erro 2";
			}
		}
		
	}

	private function getExt($file){
		return end(explode(".", $file));
	}
	
	private function validateExt($ext){
		if(isset($this->fileType[0])){
			if(!in_array($ext, $this->fileType)){
				return false;
			}
		}
		return true;
	}
	
	private function validateMaxFileSize($size){
		if($this->maxFileSize > 0){
			if($size > $this->maxFileSize){
				return false;
			}
		}
		return true;
	}
}