<?php
class Orca_Config_Ini_Create
{
	private static $iniArray;
	
	public function __construct($iniFile = null, $section = true)
	{
		$this->parseIniFile($iniFile, $section);
	}
	
	private function parseIniFile($iniFile, $section)
	{
			if(file_exists($iniFile)){
				set_error_handler(array($this, 'loadIniError'));
				$iniArray = parse_ini_file($iniFile, $section);
				restore_error_handler();
				if(!$iniArray){
					throw new Orca_Exception_Create('File "'.$iniFile.'" not parsed.', 1);
				}else{
					$this->setIniArray($this->loadIniFile($iniArray));
				}
			}else{
				throw new Orca_Exception_Create('File "'.$iniFile.'" not found.', 0);
			}
	}
	private function loadIniFile($iniArray)
	{	
		$newIniArray = array();
		$entries = array();
		
		foreach ($iniArray as $key => $value){
			foreach ($value as $sKey => $sValue){
				$index = explode('.', $sKey);
				$entries[$index[1]][$index[2]] = $sValue;
			}
			$newIniArray[$key] = $entries;
		}
		return $newIniArray;
	}
	private function setIniArray($value)
	{
		self::$iniArray = $value;
		return $this;
	}
	
	public static function getIniArray()
	{
		return self::$iniArray;
	}
	private function loadIniError()
	{
	}
}